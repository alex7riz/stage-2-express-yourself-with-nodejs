const http = require('http');
const port = 3000;
var express = require("express");
var bodyParser = require("body-parser");
var fs = require("fs");

var jsonParser = bodyParser.json();
var app = express();

const requestHandler = (request, response) => {
    console.log(request.url)
    response.end(userlist)
};

const server = http.createServer(requestHandler);

server.listen(port, (err) => {
    if (err) {
        return console.log('Error', err)
    }
    console.log('Server running in the address http://localhost:${port}')
});





app.get("/userlist", function(req, res){
      
    var content = fs.readFileSync("userlist.json", "utf8");
    var userlist = JSON.parse(content);
    res.send(userlist);
});