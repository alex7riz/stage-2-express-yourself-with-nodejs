var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require("body-parser");
var fs = require("fs");

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var jsonParser = bodyParser.json();
var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);
app.use('/users', usersRouter);

//отримання масиву всіх користувачів
app.get("/api/users", function(req, res){
      
    var content = fs.readFileSync("userlist.json", "utf8");
    var users = JSON.parse(content);
    res.send(users);
});

// отримання одного користувача по ID
app.get("/api/users/:id", function(req, res){
      
    var id = req.params.id;
    var content = fs.readFileSync("users.json", "utf8");
    var users = JSON.parse(content);
    var user = null;

    for(var i=0; i<users.length; i++){
        if(users[i].id==id){
            user = users[i];
            break;
        }
    }
    if(user){
        res.send(user);
    }
    else{
        res.status(404).send();
    }
});

// створення користувача за даними з тіла запиту
app.post("/api/users", jsonParser, function (req, res) {
     
    if(!req.body) return res.sendStatus(400);
     
    var userName = req.body.name;
    var userAge = req.body.age;
    var user = {name: userName, age: userAge};
     
    var data = fs.readFileSync("userlist.json", "utf8");
    var users = JSON.parse(data);
     
    var id = Math.max.apply(Math,users.map(function(o){return o.id;}))
    user.id = id+1;
    users.push(user);
   
    var data = JSON.stringify(users);
    fs.writeFileSync("userlist.json", data);
    res.send(user);
});

// оновлення користувача за даними з тіла запиту
app.put("/api/users", jsonParser, function(req, res){
      
    if(!req.body) return res.sendStatus(400);
     
    var userId = req.body.id;
    var userName = req.body.name;
    var userAge = req.body.age;
     
    var data = fs.readFileSync("users.json", "utf8");
    var users = JSON.parse(data);
    var user;
    for(var i=0; i<users.length; i++){
        if(users[i].id==userId){
            user = users[i];
            break;
        }
    }
    if(user){
        user.age = userAge;
        user.name = userName;
        var data = JSON.stringify(users);
        fs.writeFileSync("users.json", data);
        res.send(user);
    }
    else{
        res.status(404).send(user);
    }
});

// видалення одного користувача по ID
app.delete("/api/users/:id", function(req, res){
      
    var id = req.params.id;
    var data = fs.readFileSync("userlist.json", "utf8");
    var users = JSON.parse(data);
    var index = -1;
    for(var i=0; i<users.length; i++){
        if(users[i].id==id){
            index=i;
            break;
        }
    }
    if(index > -1){
        var user = users.splice(index, 1)[0];
        var data = JSON.stringify(users);
        fs.writeFileSync("userlist.json", data);
        res.send(user);
    }
    else{
        res.status(404).send();
    }
});

module.exports = app;